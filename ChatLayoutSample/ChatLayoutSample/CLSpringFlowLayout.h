//
//  CLSpringFlowLayout.h
//  ChatLayoutSample
//
//  Created by Stanislav Shpak on 12/14/15.
//  Copyright © 2015 Stanislav Shpak. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CLSpringFlowLayout : UICollectionViewFlowLayout

@end
